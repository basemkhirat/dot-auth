<?php

namespace Dot\Auth;

class Auth extends \Dot\Platform\Plugin
{

    protected $dependencies = [
        "users" => \Dot\Users\Users::class
    ];

    protected $providers = [
        \Dot\Auth\Providers\AuthServiceProvider::class
    ];

    protected $route_middlewares = [
        'auth' => \Dot\Auth\Middlewares\AuthMiddleware::class,
        'guest' => \Dot\Auth\Middlewares\GuestMiddleware::class,
    ];

    function install($command)
    {
        $command->call("vendor:publish", [
            "--tag" => [$this->getKey() . ".config"],
            "--force" => true
        ]);

    }


}
